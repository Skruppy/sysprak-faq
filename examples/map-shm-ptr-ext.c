// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <errno.h>
#include "util.h"


int main(char* args[], int argv) {
	sk_sem_init(1);
//{snip}
	int ext_val = 0;
	struct {
		int * ext_ptr;
	} typedef shm_data_t;
	
	int shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0600);
	pid_t cpid = fork();
	if (cpid) { attach_other_shm(); }
	shm_data_t * sd = shmat(shmid, NULL, 0);
	
	if (cpid) { // Parent
		ext_val = 1;
		sd->ext_ptr = &ext_val;
		printf("Set %i @ %p via %p\n", *sd->ext_ptr, sd->ext_ptr, &sd->ext_ptr);
		sk_sem_signal();
	} else { // Child
		sk_sem_wait();
		printf("Get %i @ %p via %p\n", *sd->ext_ptr, sd->ext_ptr, &sd->ext_ptr);
	}
//{/snip}
	
	if (cpid) {
		wait(NULL);
		shmctl(shmid, IPC_RMID, NULL);
		sk_sem_destroy();
	}
	
	return 0;
}
