// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>

/* The printf() calls before printing *p influence the outcome. Commment them
 * out to produce different results.
 */
int main(char* args[], int argv) {
//{snip}
	char * p;
	printf("&p = %p\n", &p);
	printf(" p = %p\n",  p);
	printf("*p = %x\n", *p); // may crash
	printf("DONE\n"); // may be executed
//{/snip}
	
	return 0;
}
