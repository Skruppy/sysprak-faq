// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <errno.h>


void attach_other_shm() {
	int shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0600);
	shmat(shmid, NULL, 0);
	shmctl(shmid, IPC_RMID, NULL);
}

int main(char* args[], int argv) {
	int sem_shmid = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0600);
	sem_t * sem = shmat(sem_shmid, NULL, 0);
	shmctl(sem_shmid, IPC_RMID, NULL);
	sem_init(sem, 1, 1);
	
//{snip}
	// the folloing code is executed by a single process
	int shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0600);
	pid_t cpid = fork();
	// the folloing code is executed by two parallel running processes
	if (cpid) { attach_other_shm(); } // now, the two `i` will be different
	int * i = shmat(shmid, NULL, 0);
	*i = cpid ? 2 : 1;
	printf("after:  %i @ %p (pid: %i)\n", *i, i, getpid());
	sleep(1);
	printf("still:  %i @ %p (pid: %i)\n", *i, i, getpid());
//{/snip}
	
	while(sem_wait(sem) == -1 && errno == EINTR);
	printf("\nmapping (pid: %i)\n", getpid());
	FILE * f = fopen("/proc/self/maps", "r");
	char c;
	while ((c = fgetc(f)) != EOF) {
		putchar(c);
	}
	fclose(f);
	fflush(stdout);
	sem_post(sem);
	
	
	if (cpid) {
		wait(NULL);
		shmctl(shmid, IPC_RMID, NULL);
		sem_destroy(sem);
	}
	
	return 0;
}
