// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <stdint.h>

int main(char* args[], int argv) {
//{snip}
	uint16_t a[3] = { // declare, allocate init array
		0x1b1a, 0x2b2a, 0x3b3a
	};
	printf("&a = %p / a = %p / *a = 0x%x\n", &a, a, *a);
	printf("sizeof: a=%zi, *a=%zi\n\n", sizeof(a), sizeof(*a));
	printf("i *(a+i) == a[i] @ a+i == &a[i]\n");
	for(int i = 0; i < 3; i++) {
		printf("%i:  0x%x == 0x%x @ %p == %p\n", i, *(a+i), a[i], a+i, &a[i]);
	}
//{/snip}
	
	return 0;
}
