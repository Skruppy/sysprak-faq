// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main(char* args[], int argv) {
//{snip}
	uint16_t * a; // declare and allocate pointer on the stack
	a = malloc(3*2); // allocate array on the heap (skipping return check)
	for(int i = 0; i < 3; i++) { // init array
		a[i] = 0x0b0a + (i+1<<4) + (i+1<<12);
	}
	printf("&a = %p / a = %p / *a = 0x%x\n", &a, a, *a);
	printf("sizeof: a=%zi, *a=%zi\n\n", sizeof(a), sizeof(*a));
	printf("i *(a+i) == a[i] @ a+i == &a[i]\n");
	for(int i = 0; i < 3; i++) {
		printf("%i:  0x%x == 0x%x @ %p == %p\n", i, *(a+i), a[i], a+i, &a[i]);
	}
//{/snip}
	
	return 0;
}
