// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>

int main(char* args[], int argv) {
//{snip}
	int i;
	int *p2 = &i;
	int **p1 = &p2;
	i = 0x42;
	printf("Addr:   p1=%p  p2=%p  i=%p\n", &p1, &p2, &i);
	printf("Val:    p1=%p  p2=%p  i=%x\n", p1, p2, i);
	printf("Deref: *p1=%p  **p1=%x  *p2=%x\n\n", *p1, **p1, *p2);
	i = 0xff; printf("Set i to 0xff\n");
	printf("Addr:   p1=%p  p2=%p  i=%p\n", &p1, &p2, &i);
	printf("Val:    p1=%p  p2=%p  i=%x\n", p1, p2, i);
	printf("Deref: *p1=%p  **p1=%x  *p2=%x\n", *p1, **p1, *p2);
//{/snip}
	
	return 0;
}
