// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <stdlib.h>


int main(char* args[], int argv) {
//{addresses}
	static const int rodata = 42;
	static int bss;
	static int data = 42;
	void * heap = malloc(1);
	int stack = 42;
	printf("In .text:   %p\n", &main);
	printf("In .rodata: %p\n", &rodata);
	printf("In .bss:    %p\n", &bss);
	printf("In .data:   %p\n", &data);
	printf("On heap:    %p\n", heap);
	printf("On stack:   %p\n", &stack);
	free(heap);
//{/addresses}
	
//{mapping}
	printf("\nmapping:\n");
	FILE * f = fopen("/proc/self/maps", "r");
	char c;
	while ((c = fgetc(f)) != EOF) {
		putchar(c);
	}
	fclose(f);
//{/mapping}
	
	return 0;
}
