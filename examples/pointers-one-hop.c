// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>

int main(char* args[], int argv) {
//{snip}
	int *p, i;
	p = &i;
	i = 0x42;
	printf("Addr:   p=%p  i=%p\n", &p, &i);
	printf("Val:    p=%p  i=%x\n", p, i);
	printf("Deref: *p=%x\n\n", *p);
	i = 0xff; printf("Set i to 0xff\n");
	printf("Addr:   p=%p  i=%p\n", &p, &i);
	printf("Val:    p=%p  i=%x\n", p, i);
	printf("Deref: *p=%x\n\n", *p);
//{/snip}
	
	return 0;
}
