// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>

int main(char* args[], int argv) {
//{snip}
	char * p;
	printf("&p = %p\n", &p);
	printf(" p = %p\n",  p);
	printf("DONE\n");
//{/snip}
	
	return 0;
}
