// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

void attach_other_shm() {
	int shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0600);
	shmat(shmid, NULL, 0);
	shmctl(shmid, IPC_RMID, NULL);
}


sem_t * sk_sem_shm;

void sk_sem_init(int value) {
	int shmid = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0600);
	sk_sem_shm = shmat(shmid, NULL, 0);
	shmctl(shmid, IPC_RMID, NULL);
	sem_init(sk_sem_shm, 1, value);
}

void sk_sem_wait() {
	while(sem_wait(sk_sem_shm) == -1 && errno == EINTR);
}

void sk_sem_signal() {
	sem_post(sk_sem_shm);
}

void sk_sem_destroy() {
	sem_destroy(sk_sem_shm);
}
