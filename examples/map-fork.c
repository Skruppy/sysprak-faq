// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(char* args[], int argv) {
//{snip}
	// the folloing code is executed by a single process
	int i = 0;
	printf("before: %i @ %p (pid: %i)\n", i, &i, getpid());
	pid_t cpid = fork();
	// the folloing code is executed by two parallel running processes
	i = cpid ? 2 : 1;
	printf("after:  %i @ %p (pid: %i)\n", i, &i, getpid());
	sleep(1);
	printf("still:  %i @ %p (pid: %i)\n", i, &i, getpid());
//{/snip}
	
	if (cpid) {
		wait(NULL);
	}
	
	return 0;
}
