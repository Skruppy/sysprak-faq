// This file is part of Sysprak FAQ <https://gitlab.com/Skrupellos/sysprak-faq>
// Copyright (c) Skruppy <skruppy@onmars.eu>
// SPDX-License-Identifier: Apache-2.0

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main(char* args[], int argv) {
//{snip}
	uint16_t ** a = malloc(2 * sizeof(uint16_t *));
	for(int i = 0; i < 2; i++) {
		a[i] = malloc(3 * sizeof(uint16_t));
	}
	for(int y = 0; y < 2; y++) {
		for(int x = 0; x < 3; x++) {
			a[y][x] = 0x0b0a + (x+1<<4) + (y+1<<12);
		}
	}
	printf("&a = %p / a = %p / *a = %p / **a = 0x%x\n", &a, a, *a, **a);
	printf("sizeof: a=%zi, *a=%zi, **a=%zi\n\n", sizeof(a), sizeof(*a), sizeof(**a));
	printf("i *(a+i) == a[i] @ a+i == &a[i]\n");
	for(int y = 0; y < 2; y++) {
		for(int x = 0; x < 3; x++) {
			printf("a[%i][%i]=0x%x @ %p %p %p %p\n",
				   y, x, a[y][x], *(a+y)+x, *a+3*y+x, &a[y][x], &a[0][3*y+x]);
		}
	}
//{/snip}
	
	return 0;
}
