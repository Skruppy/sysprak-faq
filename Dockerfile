FROM alpine:3.12.0
RUN \
	apk add --no-cache \
		make \
		texlive-luatex \
		texmf-dist-latexextra \
		texmf-dist-fontsextra \
		git \
		gcc \
		musl-dev && \
	wget -O /tmp/JetBrainsMono.zip https://github.com/JetBrains/JetBrainsMono/releases/download/v2.002/JetBrainsMono-2.002.zip && \
	mkdir /usr/share/fonts/ && \
	unzip /tmp/JetBrainsMono.zip '*.ttf' -d /usr/share/fonts/ && \
	rm /tmp/JetBrainsMono.zip

ENV USER=ci
ENV HOME="/home/${USER}"
RUN adduser -D "${USER}"
USER $USER
WORKDIR $HOME
