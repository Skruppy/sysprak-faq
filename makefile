PATH             := $(shell pwd)/bin:$(PATH)
EXAMPLES_C       := $(wildcard examples/*.c)
EXAMPLES         := $(EXAMPLES_C:examples/%.c=examples/%)
CFLAGS           ?= -g -Og -fno-stack-protector -save-temps=obj -fverbose-asm
LDFLAGS          ?= -pthread


## "Virtual" targets who are not actual files (otherwise use FORCE)
.PHONY: all examples watch watch-slides clean distclean new


all: slides.pdf examples


examples: $(EXAMPLES)


.PRECIOUS: %.pdf
%.pdf: tex/tmp/%.pdf
	ln -f $< $@


.PRECIOUS: tex/tmp/%.pdf
tex/tmp/%.pdf: tex/%.tex FORCE
	tex2pdf $<


watch:
	cd tex/ && latexmk -halt-on-error -pvc -view=none slides.tex


clean:
	rm -fvR tex/tmp/


distclean: clean
	rm -fvR slides.pdf


new: distclean
	$(MAKE)


FORCE:
