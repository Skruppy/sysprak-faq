$pdf_mode = 4; ## 1=PdfLaTeX, 4=LuaLaTeX

$out_dir = "tmp";
$aux_dir = "tmp";

push @extra_lualatex_options,    "--shell-escape";
push @extra_lualatex_options,    "--file-line-error";
push @extra_lualatex_options,    "--halt-on-error";

$ENV{'SOURCE_DATE_EPOCH'}='0';
$ENV{'max_print_line'}='1000';
$ENV{'error_line'}='254';
$ENV{'half_error_line'}='238';
